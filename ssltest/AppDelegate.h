//
//  AppDelegate.h
//  ssltest
//
//  Created by j on 12/9/14.
//  Copyright (c) 2014 ModernAppsolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

