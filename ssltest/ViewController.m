//
//  ViewController.m
//  ssltest
//
//  Created by j on 12/9/14.
//  Copyright (c) 2014 ModernAppsolutions. All rights reserved.
//

#import "AFNetworking.h"
#import "ViewController.h"

#define THE_URL @"https://jsv.pw/deleteme"

@interface ViewController ()

@end

@implementation ViewController


@synthesize btn1;
@synthesize btn2;
@synthesize btn3;
@synthesize btn4;
@synthesize btn5;
@synthesize btn6;
@synthesize tv;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [btn1 addTarget:self action:@selector(Btn1Click:) forControlEvents:UIControlEventTouchUpInside];
    [btn2 addTarget:self action:@selector(Btn2Click:) forControlEvents:UIControlEventTouchUpInside];
    [btn3 addTarget:self action:@selector(Btn3Click:) forControlEvents:UIControlEventTouchUpInside];
    [btn4 addTarget:self action:@selector(Btn4Click:) forControlEvents:UIControlEventTouchUpInside];
    [btn5 addTarget:self action:@selector(Btn5Click:) forControlEvents:UIControlEventTouchUpInside];
    [btn6 addTarget:self action:@selector(Btn6Click:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// built-in network crap
-(void)Btn1Click:(id)sender
{
    NSString *completeUrlString = [NSString stringWithFormat:THE_URL];
    
    NSURL *sUrl = [NSURL URLWithString:completeUrlString];
    
    NSURLRequest* request = [NSURLRequest requestWithURL:sUrl
                                             cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                         timeoutInterval:10.0f];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         if (data.length > 0 && connectionError == nil)
         {
             NSString *txt = [NSString stringWithFormat: @"1) downloaded stuff: %@", [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] ];
             [self SetText:txt];
         }
         else
         {
             NSString *txt = [NSString stringWithFormat: @"1) error: %@", [connectionError localizedDescription]];
             [self SetText:txt];
         }
     }];
}

-(void)SetText:(NSString*)txt
{
    [tv setText:[NSString stringWithFormat:@"%@%@\n", tv.text, txt]];
    NSLog( @"%@", txt );
}

// default settings, no cert pinning
-(void)Btn2Click:(id)sender
{
    NSString *completeUrlString = [NSString stringWithFormat:THE_URL];
    
    NSURL *sUrl = [NSURL URLWithString:completeUrlString];
    
    NSURLRequest* request = [NSURLRequest requestWithURL:sUrl
                                             cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                         timeoutInterval:10.0f];
    if(!(nil != request))
    {
        NSLog( @"Failed to create request." );
        return;
    }
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSString *txt = [NSString stringWithFormat: @"2) downloaded stuff: %@", operation.responseString ];
        [self SetText:txt];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        NSString *txt = [NSString stringWithFormat: @"2) error: %@", [error localizedDescription]];
        [self SetText:txt];
    }];
    
    [operation start];
}

// no hostname verification
-(void)Btn3Click:(id)sender
{
    NSString *completeUrlString = [NSString stringWithFormat:THE_URL];
    
    NSURL *sUrl = [NSURL URLWithString:completeUrlString];
    
    NSURLRequest* request = [NSURLRequest requestWithURL:sUrl
                                             cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                         timeoutInterval:10.0f];
    if(!(nil != request))
    {
        NSLog( @"Failed to create request." );
        return;
    }
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.securityPolicy.validatesDomainName = NO;
    operation.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    operation.securityPolicy.validatesCertificateChain = YES;
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSString *txt = [NSString stringWithFormat: @"3) downloaded stuff: %@", operation.responseString ];
         [self SetText:txt];
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSString *txt = [NSString stringWithFormat: @"3) error: %@", [error localizedDescription]];
         [self SetText:txt];
     }];
    
    [operation start];
}

// cert pinning
-(void)Btn4Click:(id)sender
{
    NSString *completeUrlString = [NSString stringWithFormat:THE_URL];
    
    NSURL *sUrl = [NSURL URLWithString:completeUrlString];
    
    NSURLRequest* request = [NSURLRequest requestWithURL:sUrl
                                             cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                         timeoutInterval:10.0f];
    if(!(nil != request))
    {
        NSLog( @"Failed to create request." );
        return;
    }
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
    operation.securityPolicy.validatesDomainName = YES;
    operation.securityPolicy.validatesCertificateChain = YES;
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSString *txt = [NSString stringWithFormat: @"4) downloaded stuff: %@", operation.responseString ];
         [self SetText:txt];
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSString *txt = [NSString stringWithFormat: @"4) error: %@", [error localizedDescription]];
         [self SetText:txt];
     }];
    
    [operation start];
}

// dont validate cert chain
-(void)Btn5Click:(id)sender
{
    NSString *completeUrlString = [NSString stringWithFormat:THE_URL];
    
    NSURL *sUrl = [NSURL URLWithString:completeUrlString];
    
    NSURLRequest* request = [NSURLRequest requestWithURL:sUrl
                                             cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                         timeoutInterval:10.0f];
    if(!(nil != request))
    {
        NSLog( @"Failed to create request." );
        return;
    }
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.securityPolicy.validatesDomainName = YES;
    operation.securityPolicy.validatesCertificateChain = NO;
    operation.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSString *txt = [NSString stringWithFormat: @"5) downloaded stuff: %@", operation.responseString ];
         [self SetText:txt];
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSString *txt = [NSString stringWithFormat: @"5) error: %@", [error localizedDescription]];
         [self SetText:txt];
     }];
    
    [operation start];
}



-(void)Btn6Click:(id)sender
{
    NSString *completeUrlString = [NSString stringWithFormat:THE_URL];
    
    NSURL *sUrl = [NSURL URLWithString:completeUrlString];
    
    NSURLRequest* request = [NSURLRequest requestWithURL:sUrl
                                             cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                         timeoutInterval:10.0f];
    if(!(nil != request))
    {
        NSLog( @"Failed to create request." );
        return;
    }
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    operation.securityPolicy.validatesCertificateChain = NO;
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSString *txt = [NSString stringWithFormat: @"6) downloaded stuff: %@", operation.responseString ];
         [self SetText:txt];
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSString *txt = [NSString stringWithFormat: @"6) error: %@", [error localizedDescription]];
         [self SetText:txt];
     }];
    
    [operation start];
}

@end
